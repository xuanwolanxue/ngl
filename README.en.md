# dlog

#### Description
ngl即 next generation logger，是一个参考syslog-ng以及DLT实现原理 的log系统，实现一套类似dlt协议的log系统， 同时弥补开源dlt-daemon过滤机制，尤其是远程过滤的缺陷问题（因开源dlt-daemon在将所有log数据都发送出去，然后接收端是接收了所有log数据之后，再进行过滤，这样大大增加了传输带宽, 尤其是通过以太网传输的时候）。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
