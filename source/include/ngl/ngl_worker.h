/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: ngl_worker.h
 *
 * Purpose: ngl worker implmentation
 *
 * Developer:
 *   wen.gu , 2023-4-25
 *
 * TODO:
 *
 ***************************************************************************/

#ifndef __NGL_WORKER_H__
#define __NGL_WORKER_H__

#include <stdint.h>
#include "ngl/ngl_type.h"

#ifdef __cplusplus
extern "C" {
#endif



struct _ngl_worker_s;
typedef struct _ngl_worker_s* ngl_worker_t;


/**
 * @brief a thread work function, will be not return until terminate the thread.
 * 
 * @param thd_worker [in] the worker instance, used for ngl_worker_running()
 * @param opaque [in] the user private data
 * @return void
 * @note the worker function will be like:
 *   void foo_worker(ngl_worker_t thd_worker, void* opaque)  
 *   {
 *         while(ngl_worker_running(thd_worker))
 *         {
 *               // todo something
 *         }
 *   }
 */
typedef void (*ngl_worker_func)(ngl_worker_t thd_worker, void* opaque);

/**
 * @brief a thread worker function, The 'loop' function returns every time it runs
 * 
 * @param opaque [in] the user private data
 * @return GS64: how long to wait.  > 0: will be wait a while, then run next loop , == 0: run next loop immediately, < 0: will be terninate this worker
 * @note the looper function will be like:
 *   GS64 fooo_looper(void* opaque)
 *   {
 *        // todo someting
 *        return 0; // if you want to terminate current worker, then can be: return -1
 *   }
 */
typedef int64_t (*ngl_worker_loop_func)(void* opaque);

/***************************************************************************
 * Definitions of worker api
 ***************************************************************************/
/** if worker_name is NULL, then will not set worker to thread */
ngl_worker_t ngl_worker_create(const char* worker_name,  ngl_worker_func worker, void* opaque);
ngl_worker_t ngl_worker_create2(const char* worker_name, ngl_worker_loop_func looper, void* opaque);

/** will be join(wait the terminate) and destroy the worker */
ngl_error_t ngl_worker_join(ngl_worker_t thd_worker);

ngl_error_t ngl_worker_detach(ngl_worker_t thd_worker);

ngl_bool_t ngl_worker_running(ngl_worker_t thd_worker);

ngl_error_t ngl_worker_start(ngl_worker_t thd_worker);
ngl_error_t ngl_worker_pause(ngl_worker_t thd_worker); 


#ifdef __cplusplus
}
#endif

#endif /** !__NGL_WORKER_H__ */