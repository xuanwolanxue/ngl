/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: ngl_source.h
 *
 * Purpose: ngl source plugin(as server) base API define
 *
 * Developer:
 *   wen.gu , 2023-04-13
 *
 * TODO:
 *
 ***************************************************************************/
#ifndef __NGL_SOURCE_H__
#define __NGL_SOURCE_H__

#include "ngl/ngl_message.h"
/***************************************************************************
 * Definitions of source plugin base class
 ***************************************************************************/
typedef struct _ngl_source_s ngl_source_t;


/** the callback function to receive ngl message */
typedef void (*ngl_source_on_receive)(void* opaque, ngl_message_t* msg);


struct _ngl_source_s {
    ngl_source_on_receive on_receive;
    ngl_error_t (*start)(ngl_source_t* self);
    ngl_error_t (*stop)(ngl_source_t* self);
    ngl_error_t (*send)(ngl_source_t* self, void* opaque, ngl_message_t* msg);
    void (*free_fn)(ngl_source_t* self);
};


static inline ngl_error_t ngl_source_start(ngl_source_t* self) {
    return self->start(self);
}

static inline ngl_error_t ngl_source_stop(ngl_source_t* self) {
    return self->stop(self);
}

static inline ngl_error_t ngl_source_send(ngl_source_t* self, void* opaque, ngl_message_t* msg) {
    return self->send(self, opaque, msg);
}


void ngl_source_init_instance(ngl_source_t* self, ngl_source_on_receive on_receive);
void ngl_source_free(ngl_source_t* self);


#endif /** !__NGL_SOURCE_H__ */