/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: ngl_protocol.h
 *
 * Purpose: define the message protocol of ngl with NGL protocol version 2
 *
 * Developer:
 *   wen.gu , 2022-12-26
 *
 * TODO:
 *
 ***************************************************************************/
 #ifndef __NGL_PROTOCOL_H__
 #define __NGL_PROTOCOL_H__


/***************************************************************************
 * Definitions of Base Header Information
 ***************************************************************************/
#define NGL_HTYP_VERS  0x07 /**< version number, 0x1 */
#define NGL_HTYP_WHID  0x08 /**< with host id */
#define NGL_HTYP_WACID 0x10 /**< with application id and context id */
#define NGL_HTYP_WMID  0x20 /**< with message id */
#define NGL_HTYP_WSID  0x40 /**< with session id */
#define NGL_HTYP_WPID  0x80 /**< with process id */


#define NGL_HTYP_PROTOCOL_VERSION1 0x01

#define NGL_IS_HTYP_WHID(htyp)  ((htyp) & NGL_HTYP_WHID)
#define NGL_IS_HTYP_WACID(htyp) ((htyp) & NGL_HTYP_WACID)
#define NGL_IS_HTYP_WMID(htyp)  ((htyp) & NGL_HTYP_WMID)
#define NGL_IS_HTYP_WSID(htyp)  ((htyp) & NGL_HTYP_WSID)
#define NGL_IS_HTYP_WPID(htyp)  ((htyp) & NGL_HTYP_WPID)

#define NGL_IS_HTYP_VER_MATCH(htyp) (((htyp) & NGL_HTYP_VERS) == NGL_HTYP_PROTOCOL_VERSION1)

#define NGL_HTYP_HAS_EXT_HDR(htyp) ((htyp) & 0xf8)
#define NGL_HTYP_GET_EXT_HDR_FLAGS(htyp) ((htyp) & 0xf8)
/***************************************************************************
 * Definitions of msin parameter in extended header.
 ***************************************************************************/
#define NGL_MSIN_MSTP 0x0f /**< message type */
#define NGL_MSIN_MTIN 0xf0 /**< message type info */

#define NGL_MSIN_MTIN_SHIFT 4 /**< shift right offset to get mtin value */

#define NGL_GET_MSIN_MSTP(msin) ((msin) & NGL_MSIN_MSTP)
#define NGL_GET_MSIN_MTIN(msin) (((msin) & NGL_MSIN_MTIN) >> NGL_MSIN_MTIN_SHIFT)

/***************************************************************************
 * Definitions of mstp parameter in extended header.
 ***************************************************************************/
#define NGL_MSG_TYPE_LOG   0x0 /**< Log message type */
#define NGL_MSG_TYPE_CTRL  0x1 /**< Control message type */  


/***************************************************************************
 * Definitions of msli(message log information) parameter in extended header.
 ***************************************************************************/

/** define for code implementation */
#define NGL_LOG_DEFAULT (-1)
#define NGL_LOG_OFF     0   /**< turn off Log output */

/** defined in spec */
#define NGL_LOG_FATAL   1   /**< Fatal system error */
#define NGL_LOG_ERROR   2   /**< Error */
#define NGL_LOG_WARN    3   /**< Correct behavior cannot be ensued */
#define NGL_LOG_INFO    4   /**< Informational */
#define NGL_LOG_DEBUG   5   /**< Debug */
#define NGL_LOG_VERBOSE 6   /**< Highest grade of information */


#define NGL_MSIN_LOG_FATAL   ((NGL_LOG_FATAL << NGL_MSIN_MTIN_SHIFT) | NGL_MSG_TYPE_LOG)
#define NGL_MSIN_LOG_ERROR   ((NGL_LOG_ERROR << NGL_MSIN_MTIN_SHIFT) | NGL_MSG_TYPE_LOG)
#define NGL_MSIN_LOG_WARN    ((NGL_LOG_WARN << NGL_MSIN_MTIN_SHIFT) | NGL_MSG_TYPE_LOG)
#define NGL_MSIN_LOG_INFO    ((NGL_LOG_INFO << NGL_MSIN_MTIN_SHIFT) | NGL_MSG_TYPE_LOG)
#define NGL_MSIN_LOG_DEBUG   ((NGL_LOG_DEBUG << NGL_MSIN_MTIN_SHIFT) | NGL_MSG_TYPE_LOG)
#define NGL_MSIN_LOG_VERBOSE ((NGL_LOG_VERBOSE << NGL_MSIN_MTIN_SHIFT) | NGL_MSG_TYPE_LOG)


/***************************************************************************
 * Definitions of msci(message control information) parameter in extended header.
 ***************************************************************************/
#define NGL_CTRL_REQUEST  1 /** request message for control flow */
#define NGL_CTRL_RESPONSE 2 /** response message for control flow */

#define NGL_MSIN_CTRL_REQUEST  ((NGL_CTRL_REQUEST << NGL_MSIN_MTIN_SHIFT) | NGL_MSG_TYPE_CTRL)
#define NGL_MSIN_CTRL_RESPONSE ((NGL_CTRL_RESPONSE << NGL_MSIN_MTIN_SHIFT) | NGL_MSG_TYPE_CTRL)


/***************************************************************************
 * Definitions of NGL Control response status
 ***************************************************************************/

#define NGL_CTRL_RES_OK            0x00 /**< Control message response: OK */
#define NGL_CTRL_RES_NOT_SUPPORTED 0x01 /**< Control message response: Not supported */
#define NGL_CTRL_RES_ERROR         0x02 /**< Control message response: Error */
#define NGL_CTRL_RES_ERROR_ARG     0x03 /**< Control message response: Error argument */
#define NGL_CTRL_RES_PERM_DENIED   0x04 /**< Control message response: Permission denied */
#define NGL_CTRL_RES_WARNING       0x06 /**< Control message response: warning */
#define NGL_CTRL_RES_LAST          0x07 /**< Used as max value */

/***************************************************************************
 * Definitions of NGL Control Service/Command Id
 ***************************************************************************/
enum ngl_ctrl_commands {
    NGL_CTRL_CMD_ID = 0x00,
    NGL_CTRL_CMD_ID_SET_LOG_LEVEL = 0x01, /**< set */
    NGL_CTRL_CMD_ID_GET_LOG_LEVEL = 0x02,
    NGL_CTRL_CMD_ID_SET_DEFAULT_LOG_LEVEL = 0x03,
    NGL_CTRL_CMD_ID_GET_DEFAULT_LOG_LEVEL = 0x04,
    NGL_CTRL_CMD_ID_GET_SOFTWARE_VERSION  = 0x05,
    NGL_CTRL_CMD_ID_GET_HARDWARE_VERSION  = 0x06,
    NGL_CTRL_CMD_ID_BUFFER_OVERFLOW_NOTIFICATION = 0x07,
};




 #endif /**__NGL_PROTOCOL_H__ */
