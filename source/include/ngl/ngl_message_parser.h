/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: ngl_message_parser.h
 *
 * Purpose: implementation a parser to parse received data and output message instance
 *
 * Developer:
 *   wen.gu , 2023-04-07
 *
 * TODO:
 *
 ***************************************************************************/
 #ifndef __NGL_MESSAGE_PARSER_H__
 #define __NGL_MESSAGE_PARSER_H__

#include <stdint.h>

#include "ngl/ngl_message.h"

typedef enum _ngl_msg_parser_state_e {
    NGL_MsgParserStateSyncStart = 0,    /** check sync start code, then switch to 'BaseHeader' */
    NGL_MsgParserStateBaseHeader,       /** receive and parse Header, then if has extension header, then swith to 
                                          'ExtensionHeader', if hasn't, then switch to 'Payload' */
    NGL_MsgParserStateExtensionHeader,  /** receive and parse Header, then switch to 'Payload' */
    NGL_MsgParserStatePayload,          /** receive payload data, then switch to 'End' */
    NGL_MsgParserStateEnd,    /** check end code, if success then do callback. both success and fail then siwtch to 'Start' */
} ngl_msg_parser_state_t;


typedef void (*ngl_message_receive_handler)(void* opaque, ngl_message_t* msg);

typedef struct _ngl_message_parser_s {
    void* opaque;
    ngl_msg_parser_state_t parse_state;
    uint16_t header_read_pos;
    uint16_t ext_hdr_read_pos;
    uint16_t ext_hdr_size;
    uint16_t payload_read_pos;
    uint16_t payload_size;
    ngl_message_t* msg;
    ngl_message_receive_handler on_receive;
    uint8_t base_header_buf[sizeof(ngl_base_header_t)];

} ngl_message_parser_t;

#ifdef __cplusplus
extern "C" {
#endif

ngl_error_t ngl_msg_parser_initialize(ngl_message_parser_t* parser, ngl_message_receive_handler on_receive, void* opaque);
ngl_error_t ngl_msg_parser_deinitialize(ngl_message_parser_t* parser);

ngl_error_t ngl_msg_parser_reset(ngl_message_parser_t* parser);


ngl_error_t ngl_msg_parser_fill_data(ngl_message_parser_t* parser, const uint8_t* data, uint32_t size);

#ifdef __cplusplus
}
#endif

 #endif /** !__NGL_MESSAGE_PARSER_H__ */