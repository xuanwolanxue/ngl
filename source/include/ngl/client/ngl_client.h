/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: ngl_client.h
 *
 * Purpose: ngl client plugin(as client) base API define
 *
 * Developer:
 *   wen.gu , 2023-04-28
 *
 * TODO:
 *
 ***************************************************************************/
#ifndef __NGL_CLIENT_H__
#define __NGL_CLIENT_H__

#include "ngl/ngl_message.h"
#include "ngl/transport/ngl_transport.h"

#define NGL_SERVER_MSG_BROADCAST_OPAQUE (NULL)
/***************************************************************************
 * Definitions of client plugin base class
 ***************************************************************************/
typedef struct _ngl_client_s ngl_client_t;


/** the callback function to receive ngl message, 
 * user_opaque: the reference of user data pointer
 * msg: the message instance ,this hava to do release after use.
 * */
typedef void (*ngl_client_on_receive)(void* user_opaque, ngl_message_t* msg);


struct _ngl_client_s {
    void* user_opaque;
    ngl_client_on_receive on_receive;
    ngl_error_t (*connect)(ngl_client_t* self);
    ngl_error_t (*disconnect)(ngl_client_t* self);
    /** if  opaque is NGL_SERVER_MSG_BROADCAST_OPAQUE, then indicate broad cast current message */
    ngl_error_t (*send)(ngl_client_t* self, ngl_message_t* msg);
    void (*free_fn)(ngl_client_t* self);
};

#ifdef __cplusplus
extern "C" {
#endif


static inline ngl_error_t ngl_client_connect(ngl_client_t* self) {
    return self->connect(self);
}

static inline ngl_error_t ngl_client_disconnect(ngl_client_t* self) {
    return self->disconnect(self);
}

static inline ngl_error_t ngl_client_send(ngl_client_t* self, ngl_message_t* msg) {
    return self->send(self, msg);
}

void ngl_client_init_instance(ngl_client_t* self, ngl_client_on_receive on_receive, void* user_opaque);


void ngl_client_free_method(ngl_client_t* self);
void ngl_client_free(ngl_client_t* self);


#ifdef __cplusplus
}
#endif

#endif /** !__NGL_CLIENT_H__ */