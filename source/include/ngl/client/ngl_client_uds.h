/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: ngl_client_uds.h
 *
 * Purpose: implementation ngl client with unix domain socket
 *
 * Developer:
 *   wen.gu , 2023-04-28
 *
 * TODO:
 *
 ***************************************************************************/
#ifndef __NGL_SERVER_UDS_H__
#define __NGL_SERVER_UDS_H__

#include "ngl/client/ngl_client.h"

#ifdef __cplusplus
extern "C" {
#endif

ngl_client_t* ngl_client_uds_new(const char* url, ngl_client_on_receive on_receive, void* user_opaque);


#ifdef __cplusplus
}
#endif

#endif /** !__NGL_SERVER_UDS_H__ */