/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: ngl_trsnsport_unix_domain_socket.h
 *
 * Purpose: unix domain socket transport plugin implementation
 *
 * Developer:
 *   wen.gu , 2023-04-13
 *
 * TODO:
 *
 ***************************************************************************/
#ifndef __NGL_TRANSPORT_UNIX_DOMAIN_SOCKET_H__
#define __NGL_TRANSPORT_UNIX_DOMAIN_SOCKET_H__
#include <stdint.h>

#include "ngl/transport/ngl_transport.h"



#ifdef __cplusplus
extern "C" {
#endif



/***************************************************************************
 * Definitions of the API of unix domain socket transport plugin
 ***************************************************************************/

ngl_transport_t* ngl_transport_unix_domain_socket_new(int32_t fd);



#ifdef __cplusplus
}
#endif

#endif /** !__NGL_TRANSPORT_UNIX_DOMAIN_SOCKET_H__ */
