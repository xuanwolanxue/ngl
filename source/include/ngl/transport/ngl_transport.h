/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: ngl_trsnsport.h
 *
 * Purpose: ngl transport plugin base API define
 *
 * Developer:
 *   wen.gu , 2023-04-12
 *
 * TODO:
 *
 ***************************************************************************/
#ifndef __NGL_TRANSPORT_H__
#define __NGL_TRANSPORT_H__
#include <stdint.h>

#include "ngl/ngl_type.h"


#define NGL_TRANSPORT_INVALID_FD (-1)

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _ngl_transport_s ngl_transport_t;


/***************************************************************************
 * Definitions of transport plugin base class
 ***************************************************************************/
struct _ngl_transport_s {
    int32_t fd;
    ngl_transport_t* (*process_client_connection)(ngl_transport_t* self, uint32_t timeout_ms);
    int32_t (*read)(ngl_transport_t* self, uint8_t* buf, uint32_t size);
    int32_t (*write)(ngl_transport_t* self, const uint8_t* buf, uint32_t size);
    uint32_t (*max_package_size)(ngl_transport_t* self); /** get the max size of a package for write and read, if 0, mean unlimit  */
    void (*free_fn)(ngl_transport_t* self);
} ;

/***************************************************************************
 * Definitions of the API of transport base class
 ***************************************************************************/

static inline int32_t ngl_transport_read(ngl_transport_t* self, uint8_t* buf, uint32_t size) {
    return self->read(self, buf, size);
}

static inline int32_t ngl_transport_write(ngl_transport_t* self, const uint8_t* buf, uint32_t size) {
    return self->write(self, buf, size);
}

static inline ngl_transport_t* ngl_transport_process_client_connection(ngl_transport_t* self, uint32_t timeout_ms) {
    return self->process_client_connection(self, timeout_ms);
}

static inline uint32_t ngl_transport_max_package_size(ngl_transport_t* self) {
    return self->max_package_size(self);
}



void ngl_transport_init_instance(ngl_transport_t* self, int32_t fd);
void ngl_transport_free(ngl_transport_t* self);
void ngl_transport_free_default(ngl_transport_t* self);
uint32_t ngl_transport_max_package_size_default(ngl_transport_t* self);
int32_t ngl_transport_release_fd(ngl_transport_t* self);



#ifdef __cplusplus
}
#endif

#endif /** !__NGL_TRANSPORT_H__ */
