/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: ngl_type.h
 *
 * Purpose: define base type for ngl implementation
 *
 * Developer:
 *   wen.gu , 2022-12-27
 *
 * TODO:
 *
 ***************************************************************************/

 #ifndef __NGL_TYPE__
 #define __NGL_TYPE__

#include <stdint.h>


#define NGL_FALSE (0)
#define NGL_TRUE  (1)



typedef uint8_t ngl_bool_t;

/***************************************************************************
 * Definitions of NGL log level
 * Definitions of mtin(message type info) parameter for NGL_MSG_TYPE_LOG.
 ***************************************************************************/
typedef enum ngl_log_level_e {
    NGL_LOG_DEFAULT = -1,      /**< Default log level */
    NGL_LOG_OFF     = 0x00,    /**< Log level off */
    NGL_LOG_FATAL   = 0x01,    /**< fatal system error */
    NGL_LOG_ERROR   = 0x02,    /**< error with impact to correct functionality */
    NGL_LOG_WARN    = 0x03,    /**< warning, correct behaviour could not be ensured */
    NGL_LOG_INFO    = 0x04,    /**< Message of log level "Information" */
    NGL_LOG_DEBUG   = 0x05,    /**< Message of log level "Debug"  */
    NGL_LOG_VERBOSE = 0x06,    /**< highest grade of information, Message of log level "Verbose" */
    NGL_LOG_MAX                /**< maximum value, used for range check */
} ngl_log_level_t;

/***************************************************************************
 * Definitions of error type for ngl method or api
 ***************************************************************************/

typedef enum ngl_error_e {
    NGL_ErrOK = 0,
    NGL_ErrUndefined = 1,
    NGL_ErrInvalidArgument = 2,
    NGL_ErrInsufficientResources = 3,
    NGL_ErrInvalidStatus = 4,
    NGL_ErrNotFound = 5,
    NGL_ErrOutOfRange = 6,
}ngl_error_t;


/***************************************************************************
 * Definitions of instance type
 ***************************************************************************/
#if 0
typedef struct _ngl_logger_s {
    uint8_t* log_level_ptr;
    const char* ctx_id;
    uint8_t ctx_id_len;
    uint8_t mcnt;
} ngl_logger_t;
#endif

typedef void* ngl_logger_t;
typedef void* ngl_log_stream_t;


 #endif /** !__NGL_TYPE__ */