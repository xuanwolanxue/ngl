/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: ngl_message_parser22.h
 *
 * Purpose: implementation a parser2 to parse received data and output message instance
 *
 * Developer:
 *   wen.gu , 2023-04-28
 *
 * TODO:
 *
 ***************************************************************************/
 #ifndef __NGL_MESSAGE_PARSER2_H__
 #define __NGL_MESSAGE_PARSER2_H__

#include <stdint.h>

#include "ngl/ngl_message.h"

typedef enum _ngl_msg_parser2_state_e {
    NGL_MsgParser2StateSyncStart = 0,    /** check sync start code, then switch to 'BaseHeader' */
    NGL_MsgParser2StateBaseHeader,       /** receive and parse Header, then if has extension header, then swith to 
                                          'ExtensionHeader', if hasn't, then switch to 'Payload' */
    NGL_MsgParser2StateExtensionHeader,  /** receive and parse Header, then switch to 'Payload' */
    NGL_MsgParser2StatePayload,          /** receive payload data, then switch to 'End' */
    NGL_MsgParser2StateEnd,    /** check end code, if success then do callback. both success and fail then siwtch to 'Start' */
} ngl_msg_parser2_state_t;


typedef struct _ngl_message_parser2_s {
    ngl_bool_t is_got_msg;
    ngl_msg_parser2_state_t parse_state;
    uint16_t header_read_pos;
    uint16_t ext_hdr_read_pos;
    uint16_t ext_hdr_size;
    uint16_t payload_read_pos;
    uint16_t payload_size;
    ngl_message_t* msg;
    uint8_t base_header_buf[sizeof(ngl_base_header_t)];

} ngl_message_parser2_t;

#ifdef __cplusplus
extern "C" {
#endif

ngl_error_t ngl_msg_parser2_initialize(ngl_message_parser2_t* parser2);
ngl_error_t ngl_msg_parser2_deinitialize(ngl_message_parser2_t* parser2);

ngl_error_t ngl_msg_parser2_reset(ngl_message_parser2_t* parser2);


const uint8_t* ngl_msg_parser2_fill_data(ngl_message_parser2_t* parser2, ngl_message_t** msg, const uint8_t* data, const uint8_t* end_ptr);

#ifdef __cplusplus
}
#endif

 #endif /** !__NGL_MESSAGE_PARSER2_H__ */