/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: ngl_server_general.h
 *
 * Purpose: implementation ngl server plugin for default
 *
 * Developer:
 *   wen.gu , 2023-04-24
 *
 * TODO:
 *
 ***************************************************************************/
#ifndef __NGL_SERVER_GENERAL_H__
#define __NGL_SERVER_GENERAL_H__

#include "ngl/server/ngl_server.h"

#ifdef __cplusplus
extern "C" {
#endif


ngl_server_t* ngl_server_general_new(ngl_transport_t* transport, ngl_server_on_receive on_receive, void* user_opaque);

#ifdef __cplusplus
}
#endif

#endif /** !__NGL_SERVER_GENERAL_H__ */