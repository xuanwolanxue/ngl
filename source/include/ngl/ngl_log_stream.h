/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: ngl_log_stream.h
 *
 * Purpose: log stream represent a log message
 *
 * Developer:
 *   wen.gu , 2023-03-30
 *
 * TODO:
 *
 ***************************************************************************/
#ifndef __NGL_LOG_STREAM_H__
#define __NGL_LOG_STREAM_H__

#include "ngl/ngl_type.h"

#ifdef __cplusplus
extern "C" {
#endif


//if size = 0 indicate dynamic alloc size
ngl_log_stream_t ngl_log_stream_create(ngl_logger_t logger, ngl_log_level_t level, uint16_t size);
ngl_error_t ngl_log_stream_destroy(ngl_log_stream_t stream);

ngl_error_t ngl_log_stream_write_uint8(ngl_log_stream_t stream, uint8_t val);
ngl_error_t ngl_log_stream_write_uint16(ngl_log_stream_t stream, uint16_t val);
ngl_error_t ngl_log_stream_write_uint32(ngl_log_stream_t stream, uint32_t val);
ngl_error_t ngl_log_stream_write_uint64(ngl_log_stream_t stream, uint64_t val);

ngl_error_t ngl_log_stream_write_int8(ngl_log_stream_t stream,  int8_t val);
ngl_error_t ngl_log_stream_write_int16(ngl_log_stream_t stream, int16_t val);
ngl_error_t ngl_log_stream_write_int32(ngl_log_stream_t stream, int32_t val);
ngl_error_t ngl_log_stream_write_int64(ngl_log_stream_t stream, int64_t val);

ngl_error_t ngl_log_stream_write_float32(ngl_log_stream_t stream, float val);
ngl_error_t ngl_log_stream_write_float64(ngl_log_stream_t stream, double val);


ngl_error_t ngl_log_stream_write_string(ngl_log_stream_t stream, const char* val_str);


#ifdef __cplusplus
}
#endif
#endif /** !__NGL_LOG_STREAM_H__ */