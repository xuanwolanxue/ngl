/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: ngl_logger.h
 *
 * Purpose: the user side API define()
 *
 * Developer:
 *   wen.gu , 2023-02-22
 *
 * TODO:
 *
 ***************************************************************************/
#ifndef __NGL_LOGGER_H__
#define __NGL_LOGGER_H__
#include <stdint.h>
#include "ngl/ngl_type.h"

/***************************************************************************
 * Definitions of Base Header Information
 ***************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

/***************************************************************************
 * Definitions of Base Header Information
 ***************************************************************************/

ngl_error_t ngl_logger_application_initialize(const char* application_id);
ngl_error_t ngl_logger_application_deinitialize();

ngl_logger_t ngl_logger_create(const char* ctx_id, ngl_log_level_t ctx_def_level);
ngl_error_t ngl_logger_destroy(ngl_logger_t logger);

ngl_log_level_t ngl_logger_default_level(ngl_logger_t logger);
ngl_error_t ngl_logger_set_default_level(ngl_logger_t logger, ngl_log_level_t default_level);


ngl_error_t ngl_logger_write(ngl_logger_t logger, ngl_log_level_t level, const char* format, ...);
ngl_error_t ngl_logger_write_raw(ngl_logger_t logger, ngl_log_level_t level, uint8_t* buffer, uint16_t size);
ngl_error_t ngl_logger_write_stream(ngl_logger_t logger, ngl_log_stream_t stream);

#ifdef __cplusplus
}
#endif
#endif /** !__NGL_LOGGER_H__ */