/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: ngl_message_parser.c
 *
 * Purpose: implementation a parser to parse received data and output message instance
 *
 * Developer:
 *   wen.gu , 2023-04-09
 *
 * TODO:
 *
 ***************************************************************************/

#include <stdio.h>

#include "ngl/ngl_message_parser.h"
#include "ngl/ngl_message.h"
#include "ngl/ngl_protocol.h"

/***************************************************************************
 * inner function  implementation
 ***************************************************************************/
static const uint8_t* parser_on_parse_start(ngl_message_parser_t* parser, const uint8_t* data_ptr, const uint8_t* end_ptr) {
    while (data_ptr < end_ptr) {
        if (*data_ptr == NGL_MSG_SYNC_CODE) {
            data_ptr++;
            parser->header_read_pos = 0;
            parser->parse_state = NGL_MsgParserStateBaseHeader;
            return data_ptr;
        }

        data_ptr++;
        
    }/** is left data not enough( size < NGL_MSG_SYNC_CODE_LEN), then skip left data */

    return end_ptr;
}

static const uint8_t* parser_on_parse_base_header(ngl_message_parser_t* parser,  const uint8_t* data_ptr, const uint8_t* end_ptr) {
    if (parser->header_read_pos < sizeof(ngl_base_header_t)) {    
        int32_t size = (int32_t)(end_ptr - data_ptr);    
        int32_t cpSize = sizeof(ngl_base_header_t) - parser->header_read_pos;
        cpSize = (cpSize <= size) ? cpSize : size;
        memcpy(&(parser->base_header_buf[parser->header_read_pos]), data_ptr, cpSize);
        parser->header_read_pos += cpSize;
        data_ptr += cpSize;
        size -= cpSize;
    }

    if (parser->header_read_pos == sizeof(ngl_base_header_t)) {/** parse header data */
        ngl_base_header_t* base_hdr = (ngl_base_header_t*)parser->base_header_buf;

        if (base_hdr->mlen < (base_hdr->elen + sizeof(ngl_base_header_t))) {/** error length */
            parser->parse_state = NGL_MsgParserStateSyncStart;
            return data_ptr; /** todo refine me?? */
        }

        if (!NGL_IS_HTYP_VER_MATCH(base_hdr->htyp)) { /** version mismatch */
            parser->parse_state = NGL_MsgParserStateSyncStart;
            return data_ptr; /** todo refine me?? */           
        }

        if (base_hdr->mlen > sizeof(ngl_base_header_t)) {
            if (base_hdr->elen > 0) {
                if (!NGL_HTYP_HAS_EXT_HDR(base_hdr->htyp)) { /** if extension header length > 0, but haven't extension header field, then is illegal */
                    parser->parse_state = NGL_MsgParserStateSyncStart;
                    return data_ptr; /** todo refine me?? */                    
                }

                parser->ext_hdr_read_pos = 0;
                parser->ext_hdr_size = base_hdr->elen;
                parser->parse_state = NGL_MsgParserStateExtensionHeader; 
            } else {
                parser->parse_state = NGL_MsgParserStatePayload;
            }

            parser->payload_read_pos = 0;
            parser->payload_size = base_hdr->mlen - sizeof(ngl_base_header_t) - base_hdr->elen;
            parser->msg = ngl_message_create(base_hdr);

            if (!parser->msg) {
                parser->parse_state = NGL_MsgParserStateSyncStart;
                return data_ptr; /** todo refine me?? */                   
            }
           
        } else {/** this case is: length == sizeof(ngl_base_header_t) */
            parser->parse_state = NGL_MsgParserStateEnd;
        }
    }

    return data_ptr;
}

static const uint8_t* parser_on_parse_extension_header(ngl_message_parser_t* parser, const uint8_t* data_ptr, const uint8_t* end_ptr) {
    if (parser->ext_hdr_read_pos < parser->ext_hdr_size) {
        int32_t size = (int32_t)(end_ptr - data_ptr);
        int32_t cpSize = parser->ext_hdr_size - parser->ext_hdr_read_pos;
        cpSize = (cpSize <= size) ? cpSize : size;
        memcpy(ngl_message_get_extension_header_buffer(parser->msg) + parser->ext_hdr_read_pos, data_ptr, cpSize);
        parser->ext_hdr_read_pos += cpSize;
        data_ptr += cpSize;
        size -= cpSize;
    }

    if (parser->ext_hdr_read_pos == parser->ext_hdr_size) {/** parse header data */
        parser->parse_state = NGL_MsgParserStatePayload;     //todo something
    }

    return data_ptr;
}


static const uint8_t* parser_on_parse_payload(ngl_message_parser_t* parser, const uint8_t* data_ptr, const uint8_t* end_ptr) {
    if (parser->payload_read_pos < parser->payload_size) {   
        int32_t size = (int32_t)(end_ptr - data_ptr);
        int32_t cpSize = (int32_t)(parser->payload_size - parser->payload_read_pos);
        cpSize = (cpSize <= size) ? cpSize : size;
        memcpy(ngl_message_payload_buffer(parser->msg) + parser->payload_read_pos, data_ptr, cpSize);
        parser->payload_read_pos += cpSize;
        data_ptr += cpSize;
    }

    if (parser->payload_read_pos == parser->payload_size) {
        parser->parse_state = NGL_MsgParserStateEnd;
    }

    return data_ptr;
}

static const uint8_t* parser_on_parse_end(ngl_message_parser_t* parser, const uint8_t* data_ptr, const uint8_t* end_ptr) {
    //LOGD("===>: size: %d\n", size);
    if (*data_ptr == NGL_MSG_END_CODE) {
        data_ptr++;
        if (parser->msg) {
            if (parser->on_receive) {
                on_receive_(parser->opaque, parser->msg);
            } else {
                ngl_message_destroy(parser->msg);                
            }
        }
        parser->msg = NULL;
    }
    //LOGD("===>: size: %d\n", size);

    parser->parse_state = NGL_MsgParserStateSyncStart;
    return data_ptr;
}

/***************************************************************************
 * API  implementation
 ***************************************************************************/

ngl_error_t ngl_msg_parser_initialize(ngl_message_parser_t* parser, ngl_message_receive_handler on_receive, void* opaque) {
    if (!parser || !on_receive) {
        return NGL_ErrInvalidArgument;
    }

    memset(parser, 0, sizeof(ngl_message_parser_t));
    parser->parse_state = NGL_MsgParserStateSyncStart;
    parser->on_receive = on_receive;
    parser->opaque = opaque;
    return NGL_ErrOK;
}

ngl_error_t ngl_msg_parser_deinitialize(ngl_message_parser_t* parser) {
    if (parser) {
        if (parser->msg) {
            ngl_message_destroy(parser->msg);
            parser->msg = NULL;
        }
        return NGL_ErrOK;
    }

    return NGL_ErrInvalidArgument;
}

ngl_error_t ngl_msg_parser_reset(ngl_message_parser_t* parser) {
    if (!parser) {
        return NGL_ErrInvalidArgument;
    }

    if (parser->msg) {
        ngl_message_destroy(parser->msg);
        parser->msg = NULL;
    }

    ngl_message_receive_handler on_receive = parser->on_receive;
    void* opaque = parser->opaque;
    memset(parser, 0, sizeof(ngl_message_parser_t));
    parser->parse_state = NGL_MsgParserStateSyncStart;
    parser->on_receive = on_receive;  
    parser->opaque = opaque;  

    return NGL_ErrOK;
}




ngl_error_t ngl_msg_parser_fill_data(ngl_message_parser_t* parser, const uint8_t* data, uint32_t size) {
    if (!parser || !data) {
        return NGL_ErrInvalidArgument;
    }

    const uint8_t* data_ptr = data;
    const uint8_t* end_ptr = data + size;
    //LOGD("===>: size: %d\n", size);
    while (data_ptr < end_ptr) {
        switch (parser->parse_state) {
        case NGL_MsgParserStateSyncStart:
            //LOGD("===>: size: %d, start: 0x%x\n", size, *data_ptr);
            data_ptr = parser_on_parse_start(parser, data_ptr, end_ptr);
            break;
        case NGL_MsgParserStateBaseHeader:
            //LOGD("===>: size: %d, start: 0x%x\n", size, *data_ptr);
            data_ptr = parser_on_parse_base_header(parser, data_ptr, end_ptr);
            break; 
        case NGL_MsgParserStateExtensionHeader:
            data_ptr = parser_on_parse_extension_header(parser, data_ptr, end_ptr);
            break;
        case NGL_MsgParserStatePayload:
            data_ptr = parser_on_parse_payload(parser, data_ptr, end_ptr);
            //LOGD("===>: size: %d\n", size);
            break;
        case NGL_MsgParserStateEnd:
            data_ptr = parser_on_parse_end(parser, data_ptr, end_ptr);
            //LOGD("===>: size: %d\n", size);
            break;
        default:
            //LOGD("===>: size: %d\n", size);
            return NGL_ErrInvalidStatus;
            break;
        }
    }

    return NGL_ErrOK; /** todo, refineme */
    //LOGD("===>: size: %d\n", size);  

}
