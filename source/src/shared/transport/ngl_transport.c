/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: ngl_transport.c
 *
 * Purpose: ngl transport plugin base API define and implementation
 *
 * Developer:
 *   wen.gu , 2023-04-13
 *
 * TODO:
 *
 ***************************************************************************/
#include <stdlib.h>

#include "ngl/transport/ngl_transport.h"


/***************************************************************************
 * inner function  implementation
 ***************************************************************************/


/***************************************************************************
 * API function  implementation
 ***************************************************************************/
void ngl_transport_init_instance(ngl_transport_t* self, int32_t fd) {
    self->fd = fd;
    self->free_fn = ngl_transport_free_default;
}

void ngl_transport_free(ngl_transport_t* self) {
    self->free_fn(self);
    free(self);
}

void ngl_transport_free_default(ngl_transport_t* self) {
    if (self->fd != NGL_TRANSPORT_INVALID_FD) {
        close(self->fd); //todo, is need reset fd?
    }
}

int32_t ngl_transport_release_fd(ngl_transport_t* self) {
    int32_t fd = self->fd;
    self->fd = NGL_TRANSPORT_INVALID_FD;
    return fd;
}


uint32_t ngl_transport_max_package_size_default(ngl_transport_t* self) {
    return 0; /** default, unlimit max  read/write package size */
}