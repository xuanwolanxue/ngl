/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: ngl_worker.c
 *
 * Purpose: ngl worker implmentation
 *
 * Developer:
 *   wen.gu , 2023-04-25
 *
 * TODO:
 *
 ***************************************************************************/
#include <stdlib.h>
#include <pthread.h>

#include "ngl/ngl_worker.h"


/***************************************************************************
 * inner function  implementation
 ***************************************************************************/


/***************************************************************************
 * API function  implementation
 ***************************************************************************/

/** if worker_name is NULL, then will not set worker to thread */
ngl_worker_t ngl_worker_create(const char* worker_name,  ngl_worker_func worker, void* opaque);
ngl_worker_t ngl_worker_create2(const char* worker_name, ngl_worker_loop_func looper, void* opaque);

/** will be join(wait the terminate) and destroy the worker */
ngl_error_t ngl_worker_join(ngl_worker_t thd_worker);

ngl_error_t ngl_worker_detach(ngl_worker_t thd_worker);

ngl_bool_t ngl_worker_running(ngl_worker_t thd_worker);

ngl_error_t ngl_worker_start(ngl_worker_t thd_worker);
ngl_error_t ngl_worker_pause(ngl_worker_t thd_worker); 