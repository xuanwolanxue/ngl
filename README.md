# ngl

#### 介绍
ngl即 next generation logger，是一个参考syslog-ng以及DLT实现原理 的log系统，实现一套类似dlt协议的log系统， 同时弥补开源dlt-daemon过滤机制，尤其是远程过滤的缺陷问题（因开源dlt-daemon在将所有log数据都发送出去，然后接收端是接收了所有log数据之后，再进行过滤，这样大大增加了传输带宽, 尤其是通过以太网传输的时候）。

#### 软件架构
软件架构说明

#### 目录说明

```
├─doc              -- 存放文档
└─source           -- 源代码目录
    ├─include      -- 头文件
    └─src          -- 源文件
       ├─daemon    -- 实现daemon功能的源文件
       └─lib        -- dlog的公共库(libdlog)源文件
          ├─source  -- 实现log源的源文件(如来自dlog接口的log，来自某个tcp/udp地址的log等)
          ├─filter  -- 对log进行过滤的实现源码
          └─sink    -- 实现log输出目标的源文件(如输出到文件，输出到某个远方的tcp/udp地址等)

```


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
